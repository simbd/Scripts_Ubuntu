# Script de post-installation pour Ubuntu 18.04

### 1/ Aperçu

Si vous voulez un aperçu de ce que donne le script avant de le lancer vous même, voici une démo en capture vidéo via asciinema : https://asciinema.org/a/GUjWf28yzmpzHLA69N5XUd7Wp
(dans cet exemple, j'ai choisi le mode "ultimate" afin de montrer tous les choix possibles)

Astuce : si vous voulez faire le choix par défaut (qui correspond généralement à 1 et qui consiste à ne rien choisir), vous pouvez gagner du temps en faisant directement "entrée" sans saisir de chiffre sur votre clavier, ça reviendra au même.

Précision : il est fortement recommandé de mettre le terminal en "plein écran" pour le script pour un affichage optimale !
(raccourci pour lancer le terminal : ctrl + alt + T)

### 2/ Précision

- Le script de post-installation est conçu uniquement pour la dernière LTS (18.04 Bionic Beaver). Elle est sortie officiellement le 26 avril 2018. Vous pouvez récupérer l'iso ici par exemple : http://releases.ubuntu.com/18.04/

- Le script peut être utilisé avec des variantes, par exemple Xubuntu 18.04, Ubuntu Mate 18.04... à condition toutefois d'être en 64 bits (pas de support 32 bits même si la plupart des logiciels fonctionnent aussi en 32 bits). 

- Le script est compatible avec Linux Mint 19 (Cinnamon, Mate, Xfce) par contre contrairement à Ubuntu, l'utilisation des snaps ne sera pas activé par défaut (les développeurs ont fait le choix de Flatpak) donc si vous sélectionnez un paquet snap, il ne sera pas installé. Néammoins, si vous voulez quand même installer des snaps (proposé dans le script) en plus de flatpak sous Mint, il vous suffit pour cela de faire (avant de lancer le script) ceci :
```bash 
sudo apt install snapd
```

### 3/ Pour lancer le script

- C'est très simple, il suffit de télécharger le script shell (appuyer sur raw avant sur gitlab) ou directement en faisant :
```
  wget https://gitlab.com/simbd/Scripts_Ubuntu/raw/master/Ubuntu18.04_Bionic_Postinstall.sh
```
- Il suffit ensuite de mettre les droits d'éxécution : 
``` 
  chmod +x Ubuntu18.04_Bionic_Postinstall.sh
```
- Enfin pour le lancer : 
``` 
  sudo ./Ubuntu18.04_Bionic_Postinstall.sh
``` 

### 4/ Précision en cas de problème de téléchargement depuis un proxy

Si vous voulez utiliser le script depuis un environnement professionnel (par exemple en entreprise ou dans un établissement scolaire) et que vous passez derrière un proxy, peut être aurez-vous une difficulté à télécharger le script, dans ce cas il suffit de faire ceci avant :

<code>export https_proxy="ip_proxy:port_proxy"</code>

(en remplaçant ip_proxy par l'@IP de votre serveur proxy et port_proxy par le port utilisé).

Si vous avez le message suivant "Incapable d'établir une connexion SSL", il faut rajouter l'argument suivant : --no-check-certificate, ce qui donne par exemple :

<code>wget https://gitlab.com/simbd/Scripts_Ubuntu/raw/master/Ubuntu18.04_Bionic_Postinstall.sh --no-check-certificate</code>

### 5/ Utilisation

  - Le script possède différents modes adaptés suivant l'utilisateur :

#### Basique (choix 1 - manuel)

Avec ce mode ainsi que les 3 suivants, le script d'installation n'est pas entièrement automatisé, des questions vous seront posés au début pour faire un choix pour vos logiciels et ensuite le script se chargera d'installer les applications suivant vos choix. 
Cependant, ce mode comme son nom l'indique est "basique" car il va ignorer de nombreuses questions. 
Vous n'aurez pas de proposition de choix de sessions alternatives (vanilla, classique, unity...), pas de logiciel de programmation, sécurité, pas de jeux-vidéo, pas d'optimisation système, pas de customisation (thèmes, icones...), pas d'extension pour Gnome, pas de fonction serveur proposé (comme ssh-serveur par exemple), ni de paquets universels supplémentaires bref vraiment basique quoi.

#### Standard (choix 2 - manuel)

Par rapport au mode basique vous aurez 3 questions supplémentaires : le choix de session supplémentaire, des logiciels de sécurités/hacking et des jeux-vidéo. 
  
#### Avancé (choix 3 - manuel)

Par rapport au mode standard, vous aurez encore en plus : Un large choix d'extension installable si vous êtes sous Gnome, des choix de customisation (thèmes, icones, curseurs), des logiciels de programmation, des services (serveurs) activables et un choix d'optimisation système. C'est le choix que je vous recommande !
  
#### Ultimate (choix 4 - manuel)

Avec ce mode, vous aurez toutes les questions, par rapport au mode avancé, il y a encore 2 choses en plus :
Une liste de gros méta-paquets gaming installable (exemple : un gros pack de jeux d'arcades) ainsi qu'un large choix de paquets universels supplémentaires (divisés en 3 listes : snap/flatpak/appimages).
  
#### Les modes automatiques (choix 10, 11, 12...)

Contrairement aux modes manuels, vous n'aurez pas de question, les logiciels seront installés suivant le profil choisi au départ. C'est donc un mode intéressant uniquement si le profil correspond parfaitement à ce que vous avez besoin. Si vous voulez choisir vous même, il faut choisir un mode manuel qui sera beaucoup plus flexible. 
  
### 6/ Légende dans le script

- Bien que ça soit indiqué rapidement dans le script au début, un petit rappel plus complet ici concernant les éléments en couleur :

[Snap] => signifie que le logiciel ne sera pas installé avec la méthode traditionnelle (apt install...) mais avec Snap. Snappy gère notamment le multi-branche pour vos logiciels, c'est à dire que vous pouvez choisir par exemple entre plusieurs versions pour un même logiciel, par exemple VLC est dispo en snap branche stable en 3.0 mais aussi en 4.0 dans la branche edge (instable). 

Vous pouvez voir la liste de vos paquets Snappy installés via la commande suivante :
```bash 
  snap list
```  
Pour avoir des infos sur un paquet snappy notamment les versions disponibles, par exemple VLC :
```bash 
  snap info vlc
```  

Pour mettre à jour l'ensemble de vos paquets snaps :
```bash 
  sudo snap refresh
```  

[Flatpak] => cette fois-ci le logiciel sera installé avec Flatpak (une alternative aux snaps), c'est une méthode d'installation encore peu connue des Ubunteros pour 2 raisons : flatpak n'est pas installé par défaut (sauf sous Mint qui l'a adopté) et il n'est présent dans les dépots officiels que depuis les versions récentes (sous la 16.04 il fallait ajouter un PPA par exemple). Les paquets Flatpak fonctionnent particulièrement bien avec l'environnement "Gnome Shell", ça tombe bien, c'est l'environnement par défaut de la 18.04.

Pour voir les paquets flatpak installés :
```bash 
  flatpak list
```  
Pour mettre à jour l'ensemble de vos paquets flatpak :
```bash 
  sudo flatpak update -y
```  
(A noté que dans la partie optimisation, vous avez la possibilité d'avoir une commande "maj" qui met tout à jour d'un coup) c'est en faite un alias qui fait la même chose que :
```bash 
  sudo apt update && sudo apt full-upgrade -y ; sudo snap refresh ; sudo flatpak update -y
```  

[AppImage] => Le format de paquets Appimage permet de distribuer des logiciels de manière portable sur n'importe quelle distribution Linux, y compris Ubuntu. Le but est de pouvoir utiliser des applications simplement, avec une grande compatibilité, sans impacter le système. Si vous mettez un logiciel au format AppImage sur une clé usb par exemple, il pourra se lancer sur un poste sur Ubuntu, sur une autre poste sur Fedora, sur encore un autre poste sur Archlinux etc...

Cependant, sachez que contrairement aux paquets Snappy et Flatpak, les AppImages ne se mettent pas à jour. Si une nouvelle version sort du logiciel, ça sera à vous d'aller récupérer manuellement la nouvelle AppImage du logiciel en question sur le site.
  
[I!] => Signifie que l'installation ne peux pas être entièrement automatisé, autrement dit en sélectionant un logiciel avec cet avertissement, le script va s'arréter en plein milieu et vous demander d'intervenir et ne reprendra qu'une fois avoir compléter l'installation (par exemple pour accepter un contrat de licence). C'est quelque chose d'assez rare (très peu de logiciels sont concernés). 

[X!] => Signifie "Compatible uniquement en session Xorg mais pas en session Wayland". Cet avertissement ne concerne que ceux qui utilisent la version de base (donc avec Gnome Shell), si vous utilisez une variante vous n'avez pas à vous poser de question car vous êtes forcément sous Xorg (donc logiciel compatible). 
Sous le nouveau Ubuntu avec Gnome Shell, il y a 2 sessions, la session Xorg (choix par défaut) et la session Wayland (choix alternatif). Certains logiciels ne sont pas compatibles avec Wayland mais fonctionneront sous Xorg. C'est le cas par exemple de "Synaptic" ou "Gparted". Cela dit, ce n'est pas très génant dans la mesure ou Xorg est la session par défaut sous la 18.04 (contrairement à la 17.10).
Cela vient du fait que Wayland est plus sécurisé et interdit de lancer une application graphique avec les droits root, chose que demande Gparted par exemple.
A noté qu'il y a une méthode de contournement sous Wayland (cf mode avancé/extra partie optimisation, choix "commande fraude wayland" : Vous pouvez alors lancer par exemple gparted en session Wayland via la commande : fraude gparted)

[M!] => Signifie que le logiciel devra être lancé manuellement depuis le dossier présent dans votre dossier perso (il n'y en a quasiment plus car j'ai crée moi-même des raccourcis pour ceux qui étaient en manuel).

### 7/ Contribution

N'hésitez pas à contribuer, par exemple pour :
- corriger des fautes
- proposer des logiciels manquants

### 8/ Options supplémentaires

Si besoin (cas particulier), le script peut être lancé avec 2 options (paramètre) :

- paramètre 1 "vbox" : installe les additions invités, peut être utile si vous utilisez le script dans un Ubuntu virtualisé dans Virtualbox. Dans ce cas le script se lance comme ceci : sudo ./script.sh vbox

- paramètre 2 "NRI!" (NeRienInstaller!) : n'installe aucun logiciel de la base commune (déconseillé), attention n'utilisez cette option que si vous savez ce que vous faites, par exemple flatpak ne sera pas installé mais le script propose un choix de logiciel flatpak, si vous choisissez un paquet flatpak dans la liste dans ce cas ça ne fonctionnera pas et il faudra alors installer vous même flatpak manuellement avant de lancer le script. Cette option n'a d'intérêt que pour certains utilisateurs qui veulent vraiment un système très minimale sans même des logiciels utiles comme "transmission" ou "thunderbird". Dans ce cas le script se lance comme ceci : sudo ./script.sh - NRI! (ou pour cummuler les 2 : sudo ./script.sh vbox NRI!). 
